package com.sda.w1.silnia;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner ins = new Scanner(System.in);
        System.out.println("Podaj liczbe z ktorej mam wyliczyc silnie: ");
        int liczba = ins.nextInt();
        System.out.println("Wynik = "+fact(liczba));

    }

    private static int fact(int li) {
        if (li > 1) {
            return li * fact(li-1);
        } else {
            return 1;
        }
    }
}
