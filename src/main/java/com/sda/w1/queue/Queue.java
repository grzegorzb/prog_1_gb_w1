package com.sda.w1.queue;

public interface Queue<T> {
    void offer (T element);
    T pool();
    T peek();
    int size();

}
