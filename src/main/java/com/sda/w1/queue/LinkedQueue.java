package com.sda.w1.queue;

public class LinkedQueue<T> implements Queue<T> {
    private Link<T> head; // czolo kolejki

    @Override
    public void offer(T element) {
        Link<T> lnk = new Link<>(element);
        if (this.head == null) this.head = lnk;
        else {
//            Link<T> l_ost = this.head;  //prowadzacy stworzyl zmienna l_ost zamiast za każdym razem przypisywać do this.head
            while (this.head.getNext() != null) {
                this.head = this.head.getNext();
            }
            this.head.setNext(lnk);
        }
    }

    @Override
    public T pool() {
        if (this.head == null) {
            System.out.println("Empty linked queue");
            return null;
        }
        T headValue = head.getValue();
        head = head.getNext();
        return headValue;
    }

    @Override
    public T peek() {
        return (T) head.getValue();
    }

    @Override
    public int size() {
        return 0; //zrob w domu
    }
}
