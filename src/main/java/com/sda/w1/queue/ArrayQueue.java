package com.sda.w1.queue;

import java.util.Arrays;

public class ArrayQueue<T> implements Queue<T> {
    private Object[] elements;

    public ArrayQueue() {
        this.elements = new Object[0];
    }

    @Override
    public void offer(T element) {
        elements = Arrays.copyOf(elements, elements.length + 1);
        elements[elements.length - 1] = element;
    }

    @Override
    public T pool() {
        if (checkIfArrayIsEmpty()) return null;
        T t0 = (T) elements[0];
        elements[0]=null;
        this.elements = Arrays.copyOfRange(elements,1, elements.length);
//        elements = Arrays.copyOfRange(elements,1, elements.length);
        return t0;
    }

    private boolean checkIfArrayIsEmpty() {
        if (elements.length == 0) {
            System.out.println("Queue is empty");
            return true;
        }
        return false;
    }

    @Override
    public T peek() {
        if (checkIfArrayIsEmpty()) return null;
        return (T) elements[0];
    }

    @Override
    public int size() {
        return this.elements.length;
    }

    public static void main(String[] args) {
        ArrayQueue<Integer> arq = new ArrayQueue();
        arq.offer(31);
        arq.offer(32);
        arq.offer(33);
        arq.pool();
        System.out.println(arq.size() + " "+arq.peek() );

    }
}
