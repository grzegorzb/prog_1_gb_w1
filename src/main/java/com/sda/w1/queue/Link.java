package com.sda.w1.queue;

public class Link<T> {
    private T value;
    private Link<T> next;

    public Link(T val) {
        this.value=val;
    }

    public T getValue() {
        return value;
    }
//
//    public void setValue(T value) {
//        this.value = value;
//    }

    public Link<T> getNext() {
        return next;
    }

    public Link<T> setNext(Link<T> next) {
        this.next = next;
        return this;
    }

}
