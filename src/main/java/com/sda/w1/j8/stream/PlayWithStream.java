package com.sda.w1.j8.stream;


import com.sda.w1.j8.model.ContractType;
import com.sda.w1.j8.model.Employee;

import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class PlayWithStream {

    public static void main(String[] args) {
        List<Employee> employees = FileUtils.load(Path.of("src/main/resources/j8/employees.csv"));

        System.out.println(employees);
// 1 luty 2020 sobota, Zadania z https://gitlab.com/szykoc/prog1/blob/master/src/main/java/j8/README.md
//zad 1
        employees.stream()
                .filter(e -> e.getSalary() > 2500 && e.getSalary() < 3199)
                .forEach(System.out::println);

        //zad 2
        System.out.println("Zad 2");
        employees.stream()
//                .filter(e-> Math.floorMod(e.getAge(),2) ==0)
                .filter(e -> e.getAge() % 2 == 0)
                .forEach(System.out::println);
//zad 3
        ex14(employees);
//zad 4

    }

    private static void ex3(List<Employee> employees) {
        System.out.println("Zad 3 w ex3");
        employees.stream()
                .filter(e -> e.getLastName().endsWith("ska") && e.getType().equals(ContractType.F))
                .forEach(System.out::println);
    }

    private static void ex4(List<Employee> employees) {
        System.out.println("Zad 4 w ex4");
        employees.stream()
                .map(Employee::getFirstName)
                .forEach(System.out::println);
    }

    private static void ex4_drugie(List<Employee> employees) {
        System.out.println("Zad 4 ktore jest 5");
//        employees.stream()
//                .map(e -> (e.getLastName().endsWith("ski") || e.getLastName().endsWith("ska")) ? e.getLastName().substring(e.getLastName().length()-3).toUpperCase() : e.getLastName().substring(e.getLastName().length()-3))
//                .forEach(System.out::println);
        employees.stream()
                .map(Employee::getLastName)
                .map(l -> l.substring(l.length() - 3))
                .map(s -> (s.endsWith("ski") || s.endsWith("ska")) ? s.toUpperCase() : s)
                .forEach(System.out::println);
    }

    private static void ex5(List<Employee> employees) {
        System.out.println("Zad 5");
        employees
                .forEach(e -> System.out.println(e.getProfession().toUpperCase()));
    }

    private static void ex6(List<Employee> employees) {
        System.out.println("Zad 6");
        System.out.println("Czy jest osoba o nazwisku Kowalski? " + employees.stream().anyMatch(e -> e.getLastName().toUpperCase().equals("KOWALSKI")));
    }

    private static void ex7(List<Employee> employees) {
        System.out.println("Zad 7");
//        Map<String, Double> kollec1 = employees.stream()
//                .collect(toMap(e -> e.getLastName(), e -> e.getSalary()));
        Map<String, List<Double>> kollec2 = employees.stream()
                .collect(Collectors.groupingBy(e -> e.getLastName(), Collectors.mapping(e -> (e.getSalary() * 1.12), toList())));
        System.out.println(kollec2);
    }

    private static void ex8(List<Employee> employees) {
        System.out.println("Zad 8");
        employees.stream()
                .filter(e -> e.getLastName().length() > 3 && e.getLastName().isEmpty() == false)
                .filter(e -> (e.getFirstName().charAt(1) == 'a' && e.getLastName().charAt(3) == 'b'))
                .forEach(e -> System.out.println(e.getFirstName() + " " + e.getLastName()));
    }

    private static void ex9(List<Employee> employees) {
        System.out.println("Zad 9 Moje");
        employees.stream()
                .sorted((osoba1, pers2) -> String.valueOf(osoba1.getLastName() + osoba1.getFirstName()).compareTo(pers2.getLastName() + pers2.getFirstName()))
                .forEach(e -> System.out.println(e.getFirstName() + " " + e.getLastName()));
        System.out.println("Zad 9 Szymona");

        employees.stream()
                .sorted(
//                   (per1,per2) -> {  //to zamienilismy ponizej na metody referencyjne
//                    int LastNameCompar = per1.getLastName().compareTo(per2.getLastName());
//                    if (LastNameCompar!=0) return LastNameCompar;
//                    return per1.getFirstName().compareTo(per2.getFirstName());
//                                      }
                        Comparator.comparing(Employee::getLastName)
                                .thenComparing(Employee::getFirstName)
                )
                .forEach(e -> System.out.println(e.getFirstName() + " " + e.getLastName()));
    }

    private static void ex10(List<Employee> employees) {
        System.out.println("Zad 10");
        String str = employees.stream()
                .map(e -> e.getLastName())
                .collect(joining(","));
        System.out.println(str);
    }

    private static void ex11(List<Employee> employees) {
        System.out.println("Zad 11");
//        Map<Boolean, Employee> kollec2 =         employees.stream()
//                .collect( Collectors.partitioningBy(e->e.getSalary()>5000),e),toList()
//                ;
        Map<Boolean, List<Employee>> koll11 = employees.stream()
                .collect(Collectors.partitioningBy(e -> e.getSalary() > 5000));
        System.out.println("koll11= " + koll11);


        Map<Boolean, List<String>> koll12 = employees.stream()
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000,
                                Collectors.mapping(e -> e.getProfession(), toList())
                        ));
        System.out.println("koll12= " + koll12);

        final Map<Boolean, Long> koll13 = employees.stream()
                .collect(
                        Collectors.partitioningBy(
                                e -> e.getSalary() > 5000,
                                Collectors.counting())
                );
        System.out.println("koll13= " + koll13);
    }

    private static void ex12(List<Employee> employees) {
        System.out.println("Zad 12");
        final Map<String, Double> kolekc1 = employees.stream()
                .collect(groupingBy(e -> e.getType().name(), // mapping(e -> e.getSalary(), toList())));  //teraz wypisze pensje po sortowane na typ etatu
                        Collectors.averagingDouble(e -> e.getSalary())
                ));
        System.out.println(kolekc1);
    }

    private static void ex13(List<Employee> employees) {
        System.out.println("Zad 13");
        Map<String, List<String>> kolekc1 = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), mapping(e -> e.getLastName(), toList())));
        System.out.println(kolekc1);

        Map<String, List<Employee>> kolekc2 = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), toList()));
        System.out.println(kolekc2);

        Map<String, Long> kol4 = employees.stream()
                .collect(groupingBy(e -> e.getFirstName(), counting()));
        System.out.println(kol4);

        Map<String, Long> kol5 = employees.stream()
                .filter(e-> e.getFirstName().substring(e.getFirstName().length()-1).equals("a"))
                .collect(groupingBy(e -> e.getFirstName(), counting()));
        Map<String, Long> kol6 = employees.stream()
                .sorted(Comparator.comparing(Employee::getFirstName)) // w mapie i tak nie widzac sortowania
                .collect(groupingBy(e -> e.getFirstName(), filtering(e-> e.getFirstName().endsWith("a"),counting())))  //wypisuje wszystkie ale zlicza te które kończą się na a
        ;

        Map<String, Integer> kol7 = employees.stream()  // to samo co kol6 ale zwraca Integer
                .collect(groupingBy(Employee::getFirstName, collectingAndThen(counting(), value -> value.intValue())))  // collectingAndThen pobiera przeciwnie parametry jak grouping by. najpier zbiera a potem modyfikuje
                ;
        System.out.println("kol7= "+kol7);

    }

    private static void ex14(List<Employee> employees) {
        System.out.println("Zad 12");
        Optional<Employee> kolec1 = employees.stream()
                .collect(maxBy(Comparator.comparing(e -> e.getSalary())));
        System.out.println("kolec1 "+kolec1);
        Optional<Employee> egooo = employees.stream()  // optional - nie musimy martwić się czy będzie null, nie wywali się
                .filter(e -> e.getFirstName().equals("GOOO"))
                .findFirst();
        System.out.println("egooo "+egooo);

    }
}
