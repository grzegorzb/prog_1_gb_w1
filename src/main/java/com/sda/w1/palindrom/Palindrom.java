package com.sda.w1.palindrom;

import java.util.Scanner;

// palindrom to słowo czytane od konca ktore brzmi tak samo np. kajak
public class Palindrom {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj słowo: ");
        String slowo = in.nextLine();
        int slLen = slowo.length();
        System.out.println("Podane slowo ma dlugość: " + slLen);
        boolean fl = true;
        int idx = 0;
        while (slLen > 0 && fl && idx < slLen / 2) {
            if (slowo.charAt(++idx) != slowo.charAt(slLen - idx - 1)) {
                fl = false;
            }
        }
        if (slLen < 2){
            System.out.println("Slowo jest za krótkie");
        } else if (fl) {
            System.out.println("Slowo jest palindromem");
        } else {
            System.out.println("Slowo nie jest palindromem");
        }
    }
}
