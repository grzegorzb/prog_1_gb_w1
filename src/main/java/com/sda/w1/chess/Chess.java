package com.sda.w1.chess;

import java.util.Scanner;

public class Chess {


    public static final String WB_FIELDS = "\u25A0 \u25A1 ";
    public static final String BW_FIELDS = "\u25A1 \u25A0 ";

    public static void main(String[] args) {
        Scanner scin = new Scanner(System.in);
        int chess_length;
        System.out.println("Podaj dlugosc szachownicy: ");
        chess_length=scin.nextInt();
        System.out.println("Rysujesz szachownice o dlugosci "+chess_length);
//        System.out.println((int)'%'); //wypisze kod znaku
        for (int k=0;k<chess_length;k++) {
            for (int i = 0; i < chess_length ; i++) {
                System.out.print((i+k)%2==0?"%":"*");
//                System.out.print(k%2==0? WB_FIELDS : BW_FIELDS);
            }
            System.out.println("");
        }
        System.out.println(" wersja z jednym Forem");
//        for (int x=1;x<=chess_length*chess_length;x++)
//            System.out.print(((((x)/chess_length)+x)%2)+"KK"+( (((x-1)/chess_length)+x) %2==0?"\u25A1":"\u25A0")+(x%(chess_length)==0?"\n":""));
        for (int x=0;x<=(chess_length-1)*(chess_length-1);x++)
            System.out.print(((x)%2)+"KK"+( (((x-1)/chess_length)+x) %2==0?"\u25A1":"\u25A0")+(x%(chess_length)==0?"\n":""));
    }

}
