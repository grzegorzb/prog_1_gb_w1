package com.sda.w1.stack;

public class Stack1 {
    private int arrint[];
    private int top;
    private int capac;

    public Stack1(int capac) {
        this.capac = capac;
        this.arrint = new int[capac];
        this.top = -1; //przed stworzeniem jest poza tablica
    }

    public void push (int elem) {
        this.arrint[++top] = elem;
    }
    public int pop(){
        return this.arrint[top--];
    }
    public int peek(){
        return this.arrint[top] ;
    }

    private boolean isFull(){
        return top == this.capac-1;
    }

    private boolean isEmpty(){
        return top == -1;
    }
}
