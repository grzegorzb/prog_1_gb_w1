package com.sda.w1.anagram;

import java.util.Arrays;
import java.util.Scanner;

public class Anagram {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj pierwsze słowo: ");
        String slowo1 = in.nextLine();
        System.out.println("Podaj drugie słowo: ");
        String slowo2 = in.nextLine();
        if (slowo1.length()!= slowo2.length()){
            System.out.println("podane słowa nie sa anagramami");
            return;
        }
        char[] chrSl1= slowo1.toLowerCase().toCharArray();
        Arrays.sort(  chrSl1);
        char[] chrSl2= slowo2.toLowerCase().toCharArray();
        Arrays.sort(  chrSl2);
        if (Arrays.equals(chrSl1,chrSl2)) {
            System.out.println("Podane slowa sa anagramami");
        } else {
            System.out.println("Podane słowa nie sa anagramami");
        }
    }
}
