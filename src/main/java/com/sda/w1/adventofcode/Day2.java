package com.sda.w1.adventofcode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day2 {

    public static void main(String[] args) throws IOException {
        int mass = 118997;
        int fuelForOne;
        int fuelSum=0;


        fuelForOne = Math.floorDiv(mass, 3) - 2;

        System.out.println(fuelForOne);

        Stream<String> inputFile = Files.lines(Path.of("src/main/resources/adventofcode/fuel.txt"));

        List<String> listOfMass = inputFile.collect(Collectors.toList());

        for (String masa: listOfMass ) {
            fuelSum += calculateFuel(Integer.parseInt(masa));
            System.out.println(calculateFuel(Integer.parseInt(masa)));
        }
        System.out.println("fuelSum= "+fuelSum);
    }

    public static int calculateFuel(int mass) {
        int fuel = Math.floorDiv(mass, 3) - 2;
        if (fuel <= 0) {
            return 0;
        }else {
            return fuel+ calculateFuel(fuel);
        }
    }
}
