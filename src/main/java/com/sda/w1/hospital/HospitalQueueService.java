package com.sda.w1.hospital;

import java.util.PriorityQueue;
import java.util.Scanner;

public class HospitalQueueService {
    private PriorityQueue<Patient> hosQu = new PriorityQueue<>(new PatientComparator());

    public void addPatient(Patient pac) {
        this.hosQu.add(pac);
    }

    public Patient handlePatient() {
        return hosQu.poll();
    }

    public Patient nextPatient(Patient pac) {
        return hosQu.peek();
    }

    public int qSize() {
        return hosQu.size();
    }
}
