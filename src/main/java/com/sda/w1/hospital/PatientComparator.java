package com.sda.w1.hospital;

import java.util.Comparator;

public class PatientComparator implements Comparator<Patient> {
    private static final String KOWALSKI = "Kowalski";

    @Override
    public int compare(Patient o1, Patient o2) {
//        boolean isFirstKowalski = o1.getNazwisko().equals("Kowalski");
//        boolean isSecondKowalski = o2.getNazwisko().equals("Kowalski");
//        if (!isFirstKowalski && isSecondKowalski) return 1;
//        else if (isFirstKowalski && !isSecondKowalski) return -1;

        if (o1.getRozpoznanaChoroba().getZarazliwosc() > o2.getRozpoznanaChoroba().getZarazliwosc()) {
            return -1;
        } else if (o1.getRozpoznanaChoroba().getZarazliwosc() < o2.getRozpoznanaChoroba().getZarazliwosc()) {
            return 1;
        }
        return 0;
    }

}
