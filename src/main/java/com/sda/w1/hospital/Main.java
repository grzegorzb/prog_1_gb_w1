package com.sda.w1.hospital;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        HospitalQueueService hqs = new HospitalQueueService();
        Scanner scan = new Scanner(System.in);

        String opc;
        do {
            System.out.println("Witaj w rejestracji, wybierz" + "\n" +
                    "a. Rejestracja nowego pacjenta" + "\n" +
                    "b. Obsluż pacjenta" + "\n" +
                    "c. Ilość pacjentów w kolejce" + "\n" +
                    "d. Nastepny pacjent" + "\n" +
                    "q. Koniec programu");
            switch (opc = scan.nextLine()) {
                case "a":
                    System.out.println("Przyjmij pacjenta");
                    Patient nPatie = handleNewPatient(scan);
                    hqs.addPatient(nPatie);
                    break;
                case "b":
                    Patient handlPat = hqs.handlePatient();
                    printPatientInfo(handlPat);
                    break;
                case "c":
                    break;
                case "d":
                    break;
                case "q":
                    break;
                default:
                    System.out.println("Błędny wybór. Podałeś: " + opc);
            }
        } while (!opc.equals("q"));

    }

    private static void printPatientInfo(Patient handPat) {
        if (handPat != null) {
            System.out.println(new StringBuilder()
                    .append("Pacjent ")
                    .append(handPat.getImie())
                    .append(" ")
                    .append(handPat.getNazwisko())
                    .append(" zostal przyjety.")
                    .toString()
            );
        }
        else System.out.println("Koniec pacjentow.a");

    }

    private static Patient handleNewPatient(Scanner scn) {
        System.out.println("Imie: ");
        String name = scn.nextLine();
        System.out.println("Nazwisko: ");
        String surName = scn.nextLine();
        System.out.println("Złość: ");
//            String sangry = scn.nextLine(); //scn.nextInt();
        int angry = Integer.parseInt(scn.nextLine());

        System.out.println("Choroba: ");
        String disease = scn.nextLine();
        chb dis = chb.valueOf(disease);

        Patient patient = new Patient();
        patient.setImie(name);
        patient.setNazwisko(surName);
        patient.setJakBardzoZly(angry);
        patient.setRozpoznanaChoroba(dis);
        return patient;
    }
}
