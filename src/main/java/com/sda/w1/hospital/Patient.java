package com.sda.w1.hospital;

public class Patient {
    private String imie;
    private String nazwisko;
    private int jakBardzoZly;
    private chb rozpoznanaChoroba;


    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getJakBardzoZly() {
        return jakBardzoZly;
    }

    public void setJakBardzoZly(int jakBardzoZly) {
        this.jakBardzoZly = jakBardzoZly;
    }

    public chb getRozpoznanaChoroba() {
        return rozpoznanaChoroba;
    }

    public void setRozpoznanaChoroba(chb rozpoznanaChoroba) {
        this.rozpoznanaChoroba = rozpoznanaChoroba;
    }

}
