package com.sda.w1.hospital;

public enum chb {
    GRYPA(1),
    PRZEZIEBIENIE(2),
    BIEGUNKA(3),
    COS_POWAZNEGO(4);
    private int zarazliwosc;

    chb(int v) {
        this.zarazliwosc = v;
    }
    public int getZarazliwosc(){
        return this.zarazliwosc;
    }
}
