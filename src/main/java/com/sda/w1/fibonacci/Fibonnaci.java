package com.sda.w1.fibonacci;

import java.util.Scanner;

public class Fibonnaci {
    public static void main(String[] args) {
        Scanner ins = new Scanner(System.in);
        System.out.println("Podaj ktora liczbe Fibonnaciego wypisać: ");
        int liczba = ins.nextInt();
        System.out.println(liczba+" = "+Fib(liczba));

    }

    private static int Fib(int li) {
        if (li > 2) {
            return Fib(li-1) + Fib(li-2);
        } else {
            return 1;
        }
    }
}
